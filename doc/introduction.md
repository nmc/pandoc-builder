---
title: Test für pandoc-builder
titlegraphic: ./doc/img/annotated/logo.png
author: New Media Center - Universität Basel
date: \today
papersize: a4paper
toc: yes
lang: de
documentclass: report
subparagraph: yes
header-includes:
    - \usepackage{lipsum}
    - \usepackage{graphicx}
---

# Einleitung

Diese Dokumentation dient als Test für das pandoc-builder Image.

# Resultat

Herzlichen Glückwunsch, das Dokument wurde erfolgreich generiert!

# Lorem

\lipsum[2-4]

## Ipsum

\lipsum[5-8]

## Muspi

\lipsum[9-11]


# Merol

\lipsum[12-20]
