FROM nixos/nix

RUN mkdir /sandbox
WORKDIR /sandbox
RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
RUN nix-channel --update

# RUN nix-build -A pythonFull '<nixpkgs>'
RUN nix-env -i pandoc
RUN nix-env -f "<nixpkgs>" -iA texlive.combined.scheme-full
# RUN nix-env -f "<nixpkgs>" -iA texlive.combined.scheme-medium
